
#ifndef TUNNEL_JSONHELPER_HEADER
#define TUNNEL_JSONHELPER_HEADER

#include <json.hpp>
#include <types.h>
#include <glm/glm.hpp>

#include <helpers/parsegeneric.h>

namespace tunnel
{

namespace JsonHelper
{
	using Object = nlohmann::json;
	static Types::Type getType(const Object& object)
	{
		auto iter = object.find(TU_SERIALIZE_TYPE_KEY);
		if (iter != object.end())
		{
			return Types::toType(iter.value().get<std::string>());
		}
		return Types::none;
	}
	static void read(const Object& object, glm::vec2& out)
	{
		auto iterx = object.find(TU_SERIALIZE_VALUE_X);
		auto itery = object.find(TU_SERIALIZE_VALUE_Y);
		if (iterx != object.end() && iterx->is_number())
		{
			out.x = iterx->get<float>();
		}
		if (itery != object.end() && itery->is_number())
		{
			out.y = itery->get<float>();
		}
	}
	static void read(const Object& object, glm::vec3& out)
	{
		auto iterx = object.find(TU_SERIALIZE_VALUE_X);
		auto itery = object.find(TU_SERIALIZE_VALUE_Y);
		auto iterz = object.find(TU_SERIALIZE_VALUE_Z);
		if (iterx != object.end() && iterx->is_number())
		{
			out.x = iterx->get<float>();
		}
		if (itery != object.end() && itery->is_number())
		{
			out.y = itery->get<float>();
		}
		if (iterz != object.end() && iterz->is_number())
		{
			out.z = iterz->get<float>();
		}
	}
	static void read(const Object& object, glm::vec4& out)
	{
		auto iterx = object.find(TU_SERIALIZE_VALUE_X);
		auto itery = object.find(TU_SERIALIZE_VALUE_Y);
		auto iterz = object.find(TU_SERIALIZE_VALUE_Z);
		auto iterw = object.find(TU_SERIALIZE_VALUE_W);
		if (iterx != object.end() && iterx->is_number())
		{
			out.x = iterx->get<float>();
		}
		if (itery != object.end() && itery->is_number())
		{
			out.y = itery->get<float>();
		}
		if (iterz != object.end() && iterz->is_number())
		{
			out.z = iterz->get<float>();
		}
		if (iterw != object.end() && iterw->is_number())
		{
			out.w = iterw->get<float>();
		}
	}
	static void read(const Object& object, glm::ivec2& out)
	{
		auto iterx = object.find(TU_SERIALIZE_VALUE_X);
		auto itery = object.find(TU_SERIALIZE_VALUE_Y);
		if (iterx != object.end() && iterx->is_number_integer())
		{
			out.x = iterx->get<int>();
		}
		if (itery != object.end() && itery->is_number_integer())
		{
			out.y = itery->get<int>();
		}
	}
	static void read(const Object& object, glm::ivec3& out)
	{
		auto iterx = object.find(TU_SERIALIZE_VALUE_X);
		auto itery = object.find(TU_SERIALIZE_VALUE_Y);
		auto iterz = object.find(TU_SERIALIZE_VALUE_Z);
		if (iterx != object.end() && iterx->is_number_integer())
		{
			out.x = iterx->get<int>();
		}
		if (itery != object.end() && itery->is_number_integer())
		{
			out.y = itery->get<int>();
		}
		if (iterz != object.end() && iterz->is_number_integer())
		{
			out.z = iterz->get<int>();
		}
	}
	static void read(const Object& object, glm::ivec4& out)
	{
		auto iterx = object.find(TU_SERIALIZE_VALUE_X);
		auto itery = object.find(TU_SERIALIZE_VALUE_Y);
		auto iterz = object.find(TU_SERIALIZE_VALUE_Z);
		auto iterw = object.find(TU_SERIALIZE_VALUE_W);
		if (iterx != object.end() && iterx->is_number_integer())
		{
			out.x = iterx->get<int>();
		}
		if (itery != object.end() && itery->is_number_integer())
		{
			out.y = itery->get<int>();
		}
		if (iterz != object.end() && iterz->is_number_integer())
		{
			out.z = iterz->get<int>();
		}
		if (iterw != object.end() && iterw->is_number_integer())
		{
			out.w = iterw->get<int>();
		}
	}
	static Object write(int32_t value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::int32_str;
		object[TU_SERIALIZE_VALUE_KEY] = value;
		return object;
	}
	static Object write(uint32_t value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::uint32_str;
		object[TU_SERIALIZE_VALUE_KEY] = value;
		return object;
	}
	static Object write(int64_t value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::int64_str;
		object[TU_SERIALIZE_VALUE_KEY] = value;
		return object;
	}
	static Object write(uint64_t value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::uint64_str;
		object[TU_SERIALIZE_VALUE_KEY] = value;
		return object;
	}
	static Object write(bool value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::boolean_str;
		object[TU_SERIALIZE_VALUE_KEY] = value;
		return object;
	}
	static Object write(float value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::float32_str;
		object[TU_SERIALIZE_VALUE_KEY] = value;
		return object;
	}
	static Object write(double value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::float64_str;
		object[TU_SERIALIZE_VALUE_KEY] = value;
		return object;
	}
	static Object write(std::string value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::string_str;
		object[TU_SERIALIZE_VALUE_KEY] = value;
		return object;
	}
	static Object write(const glm::vec2& value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::vec2_str;
		object[TU_SERIALIZE_VALUE_X] = value.x;
		object[TU_SERIALIZE_VALUE_Y] = value.y;
		return object;
	}
	static Object write(const glm::vec3& value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::vec3_str;
		object[TU_SERIALIZE_VALUE_X] = value.x;
		object[TU_SERIALIZE_VALUE_Y] = value.y;
		object[TU_SERIALIZE_VALUE_Z] = value.z;
		return object;
	}
	static Object write(const glm::vec4& value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::vec4_str;
		object[TU_SERIALIZE_VALUE_X] = value.x;
		object[TU_SERIALIZE_VALUE_Y] = value.y;
		object[TU_SERIALIZE_VALUE_Z] = value.z;
		object[TU_SERIALIZE_VALUE_W] = value.w;
		return object;
	}
	static Object write(const glm::ivec2& value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::ivec2_str;
		object[TU_SERIALIZE_VALUE_X] = value.x;
		object[TU_SERIALIZE_VALUE_Y] = value.y;
		return object;
	}
	static Object write(const glm::ivec3& value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::ivec3_str;
		object[TU_SERIALIZE_VALUE_X] = value.x;
		object[TU_SERIALIZE_VALUE_Y] = value.y;
		object[TU_SERIALIZE_VALUE_Z] = value.z;
		return object;
	}
	static Object write(const glm::ivec4& value)
	{
		Object object;
		object[TU_SERIALIZE_TYPE_KEY] = Types::ivec4_str;
		object[TU_SERIALIZE_VALUE_X] = value.x;
		object[TU_SERIALIZE_VALUE_Y] = value.y;
		object[TU_SERIALIZE_VALUE_Z] = value.z;
		object[TU_SERIALIZE_VALUE_W] = value.w;
		return object;
	}
}
}

#endif