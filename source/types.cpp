#include <types.h>
#include <vector>
#include <util.h>

namespace tunnel
{
namespace Types
{

std::vector<std::string> typeString = {
	custom_str,
	int32_str,
	uint32_str,
	int64_str,
	uint64_str,
	boolean_str,
	float32_str,
	float64_str,
	string_str,
	vec2_str,
	vec3_str,
	vec4_str,
	ivec2_str,
	ivec3_str,
	ivec4_str,
	timestamp_str,
};

Type toType(std::string type)
{
	if(type.empty())
	{
		return none;
	}
	
	std::string rtype = toLower(type);
	for(size_t i = 0; i < typeString.size(); ++i)
	{
		if(rtype == typeString[i])
		{
			return static_cast<Type>(1 << i);
		}
	}
	return none;
}

std::string toString(Type type)
{
	if (type == none)
	{
		return "";
	}

	uint32_t value = type;
	for (size_t i = 0; value != 0 && i < typeString.size(); ++i, value >> 1)
	{
		if (value & 1)
		{
			return typeString[i];
		}
	}
	return "";
}

}
}
