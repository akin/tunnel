
#ifndef TUNNEL_VARMAP_HEADER
#define TUNNEL_VARMAP_HEADER

#include "IVar.h"
#include <string>
#include <unordered_map>
#include <functional>

namespace tunnel
{
// Case insensitive Variable Map
class VarMap
{
private:
	std::unordered_map<std::string, IVar*> m_value;
public:
	bool has(std::string key) const;
	Types::Type getType(std::string key) const;

	void setVar(const std::string& key, IVar *value);
	IVar *getVar(const std::string& key) const;

	void apply(std::function<void(const std::string&, IVar *)> function);
	void apply(std::function<void(const std::string&, const IVar *)> function) const;

	template <class IVarType> IVarType *get(std::string key) const
	{
		IVar *var = getVar(key);
		if (var == nullptr | var->getType() != IVarType::getClassType())
		{
			return nullptr;
		}
		return static_cast<IVarType*>(var);
	}

	template <class CVarType> CVarType *set(std::string key, CVarType *value)
	{
		setVar(key, value);
		return value;
	}
};
}

#endif
