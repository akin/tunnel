
#ifndef TUNNEL_PARSERVARJSON_HEADER
#define TUNNEL_PARSERVARJSON_HEADER

#include "../varmap.h"
#include <vector>

namespace tunnel
{

class Parser
{
public:
	static bool json(const std::vector<char>& data, VarMap& vars);
	static std::vector<char> writejson(const VarMap& vars);

	static bool args(int argc, char **argv, VarMap& vars);
};

}

#endif
