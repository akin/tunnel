
#include <helpers/parservar.h>
#include <json.hpp>
#include <helpers/parsegeneric.h>
#include <helpers/jsonhelper.h>
#include <var.h>
#include <platform.h>

namespace tunnel
{
namespace VarJsonParsers
{
	using Object = nlohmann::json;

	////// VAR parsers
	static IVar *read(std::string& key, VarInt32 *item, const Object& object)
	{
		auto iter = object.find(TU_SERIALIZE_VALUE_KEY);
		if (iter != object.end() && iter->is_number())
		{
			item->set(iter->get<int32_t>());
		}
		else
		{
			LOG_REPORT(LogLevel::Warning, "%s:%i Failed to parse data. [%s]", __FILE__, __LINE__, key.c_str());
		}
		return item;
	}
	static IVar *read(std::string& key, VarUInt32 *item, const Object& object)
	{
		auto iter = object.find(TU_SERIALIZE_VALUE_KEY);
		if (iter != object.end() && iter->is_number_unsigned())
		{
			item->set(iter->get<uint32_t>());
		}
		else
		{
			LOG_REPORT(LogLevel::Warning, "%s:%i Failed to parse data. [%s]", __FILE__, __LINE__, key.c_str());
		}
		return item;
	}
	static IVar *read(std::string& key, VarInt64 *item, const Object& object)
	{
		auto iter = object.find(TU_SERIALIZE_VALUE_KEY);
		if (iter != object.end() && iter->is_number())
		{
			item->set(iter->get<int64_t>());
		}
		else
		{
			LOG_REPORT(LogLevel::Warning, "%s:%i Failed to parse data. [%s]", __FILE__, __LINE__, key.c_str());
		}
		return item;
	}
	static IVar *read(std::string& key, VarUInt64 *item, const Object& object)
	{
		auto iter = object.find(TU_SERIALIZE_VALUE_KEY);
		if (iter != object.end() && iter->is_number())
		{
			item->set(iter->get<uint64_t>());
		}
		else
		{
			LOG_REPORT(LogLevel::Warning, "%s:%i Failed to parse data. [%s]", __FILE__, __LINE__, key.c_str());
		}
		return item;
	}
	static IVar *read(std::string& key, VarBool *item, const Object& object)
	{
		auto iter = object.find(TU_SERIALIZE_VALUE_KEY);
		if (iter != object.end() && iter->is_boolean())
		{
			item->set(iter->get<bool>());
		}
		else
		{
			LOG_REPORT(LogLevel::Warning, "%s:%i Failed to parse data. [%s]", __FILE__, __LINE__, key.c_str());
		}
		return item;
	}
	static IVar *read(std::string& key, VarFloat32 *item, const Object& object)
	{
		auto iter = object.find(TU_SERIALIZE_VALUE_KEY);
		if (iter != object.end() && iter->is_number())
		{
			item->set(iter->get<float>());
		}
		else
		{
			LOG_REPORT(LogLevel::Warning, "%s:%i Failed to parse data. [%s]", __FILE__, __LINE__, key.c_str());
		}
		return item;
	}
	static IVar *read(std::string& key, VarFloat64 *item, const Object& object)
	{
		auto iter = object.find(TU_SERIALIZE_VALUE_KEY);
		if (iter != object.end() && iter->is_number())
		{
			item->set(iter->get<double>());
		}
		else
		{
			LOG_REPORT(LogLevel::Warning, "%s:%i Failed to parse data. [%s]", __FILE__, __LINE__, key.c_str());
		}
		return item;
	}
	static IVar *read(std::string& key, VarString *item, const Object& object)
	{
		auto iter = object.find(TU_SERIALIZE_VALUE_KEY);
		if (iter != object.end() && iter->is_string())
		{
			item->set(iter->get<std::string>());
		}
		else
		{
			LOG_REPORT(LogLevel::Warning, "%s:%i Failed to parse data. [%s]", __FILE__, __LINE__, key.c_str());
		}
		return item;
	}
	static IVar *read(std::string& key, VarVec2 *item, const Object& object)
	{
		glm::vec2 value(0);
		JsonHelper::read(item, value);

		item->set(value);
		return item;
	}
	static IVar *read(std::string& key, VarVec3 *item, const Object& object)
	{
		glm::vec3 value(0);
		JsonHelper::read(item, value);

		item->set(value);
		return item;
	}
	static IVar *read(std::string& key, VarVec4 *item, const Object& object)
	{
		glm::vec4 value(0);
		JsonHelper::read(item, value);

		item->set(value);
		return item;
	}
	static IVar *read(std::string& key, VarIVec2 *item, const Object& object)
	{
		glm::ivec2 value(0);
		JsonHelper::read(item, value);

		item->set(value);
		return item;
	}
	static IVar *read(std::string& key, VarIVec3 *item, const Object& object)
	{
		glm::ivec3 value(0);
		JsonHelper::read(item, value);

		item->set(value);
		return item;
	}
	static IVar *read(std::string& key, VarIVec4 *item, const Object& object)
	{
		glm::ivec4 value(0);
		JsonHelper::read(item, value);

		item->set(value);
		return item;
	}
};

bool Parser::json(const std::vector<char>& data, VarMap& vars)
{
	if(data.empty()) 
	{
		LOG_REPORT(LogLevel::Message, "%s:%i Provided data was empty.", __FILE__, __LINE__);
		return true;
	}

	VarJsonParsers::Object object;

	// data contains the structure..
	try {
		object = JsonHelper::Object::parse(data.begin(), data.end());
	}
	catch (...)
	{
		LOG_REPORT(LogLevel::Message, "%s:%i Failed to parse data.", __FILE__, __LINE__);
		return false;
	}
	for (auto it = object.begin(); it != object.end(); ++it) 
	{
		std::string key = it.key();
		JsonHelper::Object value = it.value();

		Types::Type type = JsonHelper::getType(value);
		IVar *const var = vars.getVar(key);

		// Skip unkown variables!
		if (var == nullptr)
		{
			LOG_REPORT(LogLevel::Message, "%s:%i Unkown variable [%s].", __FILE__, __LINE__, key.c_str());
			continue;
		}

		if (var->getType() != type)
		{
			std::string fromType = Types::toString(type);
			std::string toType = Types::toString(var->getType());

			LOG_REPORT(LogLevel::Warning, "%s:%i Trying to parse [%s] %s to %s.", __FILE__, __LINE__, key.c_str(), fromType.c_str(), toType.c_str());
			type = var->getType();
		}
		if (type == Types::none)
		{
			continue;
		}

		IVar *newVar = nullptr;
#       define TU_PVJ_ITEM(a) case a::types_type: VarJsonParsers::read(key, static_cast<a*>(var) , value); break
		switch (type)
		{
			TU_PVJ_ITEM(VarInt32);
			TU_PVJ_ITEM(VarUInt32);
			TU_PVJ_ITEM(VarInt64);
			TU_PVJ_ITEM(VarUInt64);
			TU_PVJ_ITEM(VarBool);
			TU_PVJ_ITEM(VarFloat32);
			TU_PVJ_ITEM(VarFloat64);
			TU_PVJ_ITEM(VarString);
			TU_PVJ_ITEM(VarVec2);
			TU_PVJ_ITEM(VarVec3);
			TU_PVJ_ITEM(VarVec4);
			TU_PVJ_ITEM(VarIVec2);
			TU_PVJ_ITEM(VarIVec3);
			TU_PVJ_ITEM(VarIVec4);
		}
#       undef TU_PVJ_ITEM
	}
	return true;
}

std::vector<char> Parser::writejson(const VarMap& vars)
{
	VarJsonParsers::Object object;

#   define TU_PVJ_ITEM(a) case a::types_type : { const a *uvar = static_cast<const a*>(var); object[key] = JsonHelper::write(uvar->get()); break; }
	vars.apply([&object](const std::string& key, const IVar *var) {

		switch (var->getType())
		{
			TU_PVJ_ITEM(VarInt32);
			TU_PVJ_ITEM(VarUInt32);
			TU_PVJ_ITEM(VarInt64);
			TU_PVJ_ITEM(VarUInt64);
			TU_PVJ_ITEM(VarBool);
			TU_PVJ_ITEM(VarFloat32);
			TU_PVJ_ITEM(VarFloat64);
			TU_PVJ_ITEM(VarString);
			TU_PVJ_ITEM(VarVec2);
			TU_PVJ_ITEM(VarVec3);
			TU_PVJ_ITEM(VarVec4);
			TU_PVJ_ITEM(VarIVec2);
			TU_PVJ_ITEM(VarIVec3);
			TU_PVJ_ITEM(VarIVec4);
		}

	});
#   undef TU_PVJ_ITEM

	std::string dump = object.dump(2);
	std::vector<char> data(dump.begin(), dump.end());
	return data;
}
}