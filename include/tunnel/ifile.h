#ifndef TUNNEL_IFILE_HEADER
#define TUNNEL_IFILE_HEADER

#include <cstddef>

namespace tunnel
{
class IFile
{
public:
	virtual ~IFile() {}
	
	virtual bool canWrite() const { return false; }
	virtual bool canRead() const { return false; }
	
	virtual size_t write(size_t count, void *buf) { return 0; }
	virtual bool read(size_t count, void *buf) { return false; }
	
	virtual size_t setPosition(size_t at) { return 0; }
	virtual size_t getPosition() const { return 0; }

	virtual bool isGood() const { return false; }
	
	virtual size_t size() const { return 0; }
	virtual size_t remaining() const { return 0; }
};
}

#endif