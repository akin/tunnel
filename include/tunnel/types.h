
#ifndef TUNNEL_TYPES_HEADER
#define TUNNEL_TYPES_HEADER

#include <cstdint>
#include <string>

namespace tunnel
{
#define MILLIS2S 0.001f
using Millisecond = int64_t;
using Second = float;

namespace Types
{

enum Type : uint32_t
{
	none = 0,
	custom = 1 << 0,
	int32 = 1 << 1,
	uint32 = 1 << 2,
	int64 = 1 << 3,
	uint64 = 1 << 4,
	boolean = 1 << 5,
	float32 = 1 << 6,
	float64 = 1 << 7,
	string = 1 << 8,
	vec2 = 1 << 9,
	vec3 = 1 << 10,
	vec4 = 1 << 11,
	ivec2 = 1 << 12,
	ivec3 = 1 << 13,
	ivec4 = 1 << 14,
	timestamp = 1 << 15,
};

const std::string custom_str = "custom";
const std::string int32_str = "int32";
const std::string uint32_str = "uint32";
const std::string int64_str = "int64";
const std::string uint64_str = "uint64";
const std::string boolean_str = "boolean";
const std::string float32_str = "float32";
const std::string float64_str = "float64";
const std::string string_str = "string";
const std::string vec2_str = "vec2";
const std::string vec3_str = "vec3";
const std::string vec4_str = "vec4";
const std::string ivec2_str = "ivec2";
const std::string ivec3_str = "ivec3";
const std::string ivec4_str = "ivec4";
const std::string timestamp_str = "timestamp";

Type toType(std::string type);
std::string toString(Type type);
}

}

#endif
