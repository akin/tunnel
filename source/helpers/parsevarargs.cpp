 
#include <helpers/parservar.h>
#include <helpers/parsegeneric.h>
#include <var.h>
#include <platform.h>
#include <util.h>

#define TU_QUOTE_CHAR '"'

namespace tunnel
{
class VarArgParser
{
protected:
	std::vector<std::string>& m_data;

	bool isCommand(const std::string& command)
	{
		return command.front() == '-';
	}
private:
	bool readNext(size_t index, size_t& at, size_t count = 1)
	{
		if ((index + at + count) >= m_data.size())
		{
			return false;
		}
		at += count;
		return true;
	}

	size_t read(const std::string& key, VarInt32 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current))
		{
			return current;
		}
		int32_t result;
		if (!stringTo(m_data[index + current], result))
		{
			return current - 1;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarUInt32 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current))
		{
			return current;
		}
		uint32_t result;
		if (!stringTo(m_data[index + current], result))
		{
			return current - 1;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarInt64 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current))
		{
			return current;
		}
		int64_t result;
		if (!stringTo(m_data[index + current], result))
		{
			return current - 1;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarUInt64 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current))
		{
			return current;
		}
		uint64_t result;
		if (!stringTo(m_data[index + current], result))
		{
			return current - 1;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarBool *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current))
		{
			return current;
		}
		std::string value = m_data[index + current];

		bool result = false;
		if (value == "1")
		{
			result = true;
		}
		else
		{
			value = toLower(value);
			result = value == "true";
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarFloat32 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current))
		{
			return current;
		}
		float result;
		if (!stringTo(m_data[index + current], result))
		{
			return current - 1;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarFloat64 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current))
		{
			return current;
		}
		double result;
		if (!stringTo(m_data[index + current], result))
		{
			return current - 1;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarString *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current))
		{
			return current;
		}

		std::string value;
		size_t quotePos = m_data[index + current].find_first_of(TU_QUOTE_CHAR);
		if (quotePos != std::string::npos)
		{
			// quotes
			value += m_data[index + current].substr(quotePos);
			quotePos = value.find_first_of(TU_QUOTE_CHAR);
			if (quotePos != std::string::npos)
			{
				// Found last position too.. good
				// ["oolalalaldinging"]
				value = value.substr(0, quotePos - 1);
			}
			else
			{
				// no quote end.. must continue on next string part..
				// ["oohlalal, lalalalaah, asdasdas"]
				while (readNext(index, current))
				{
					quotePos = m_data[index + current].find_first_of(TU_QUOTE_CHAR);
					if (quotePos != std::string::npos)
					{
						// Found! good!
						value += m_data[index + current].substr(0, quotePos - 1);
						break;
					}
					else
					{
						// keep collecting
						value += m_data[index + current];
					}
				}
			}
		}
		else
		{
			// no quotes
			value = m_data[index + current];
		}

		var->set(value);
		return current;
	}
	size_t read(const std::string& key, VarVec2 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current , 2))
		{
			return current;
		}
		glm::vec2 result;
		if (!stringTo(m_data[index + current - 1], result.x) || !stringTo(m_data[index + current], result.y))
		{
			return current - 2;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarVec3 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current, 3))
		{
			return current;
		}
		glm::vec3 result;
		if (!stringTo(m_data[index + current - 2], result.x) || !stringTo(m_data[index + current - 1], result.y) || !stringTo(m_data[index + current], result.z))
		{
			return current - 3;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarVec4 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current, 4))
		{
			return current;
		}
		glm::vec4 result;
		if (!stringTo(m_data[index + current - 3], result.x) || !stringTo(m_data[index + current - 2], result.y) || !stringTo(m_data[index + current - 1], result.z) || !stringTo(m_data[index + current], result.w))
		{
			return current - 4;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarIVec2 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current, 2))
		{
			return current;
		}
		glm::ivec2 result;
		if (!stringTo(m_data[index + current - 1], result.x) || !stringTo(m_data[index + current], result.y))
		{
			return current - 2;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarIVec3 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current, 3))
		{
			return current;
		}
		glm::ivec3 result;
		if (!stringTo(m_data[index + current - 2], result.x) || !stringTo(m_data[index + current - 1], result.y) || !stringTo(m_data[index + current], result.z))
		{
			return current - 3;
		}
		var->set(result);
		return current;
	}
	size_t read(const std::string& key, VarIVec4 *var, size_t index)
	{
		size_t current = 0;
		if (!readNext(index, current, 4))
		{
			return current;
		}
		glm::ivec4 result;
		if (!stringTo(m_data[index + current - 3], result.x) || !stringTo(m_data[index + current - 2], result.y) || !stringTo(m_data[index + current - 1], result.z) || !stringTo(m_data[index + current], result.w))
		{
			return current - 4;
		}
		var->set(result);
		return current;
	}
public:
	VarArgParser(std::vector<std::string>& data)
	: m_data(data)
	{
	}

	void parse(VarMap& vars)
	{
		for (size_t i = 0; i < m_data.size(); ++i)
		{
			// "-" means command
			// "--" as well.. so lets just purge the leading
			std::string command = m_data[i];
			if (!isCommand(command))
			{
				LOG_REPORT(LogLevel::Message, "%s:%i Not a command [%s].", __FILE__, __LINE__, command.c_str());
				continue;
			}
			command = command.substr(command.find_first_not_of('-'));

			IVar *var = vars.getVar(command);
			if (var == nullptr)
			{
				LOG_REPORT(LogLevel::Message, "%s:%i Unkown variable [%s].", __FILE__, __LINE__, command.c_str());
				continue;
			}

#       define TU_PVJ_ITEM(a) case a::types_type: i += read(command, static_cast<a*>(var), i); break
			switch (var->getType())
			{
				TU_PVJ_ITEM(VarInt32);
				TU_PVJ_ITEM(VarUInt32);
				TU_PVJ_ITEM(VarInt64);
				TU_PVJ_ITEM(VarUInt64);
				TU_PVJ_ITEM(VarBool);
				TU_PVJ_ITEM(VarFloat32);
				TU_PVJ_ITEM(VarFloat64);
				TU_PVJ_ITEM(VarString);
				TU_PVJ_ITEM(VarVec2);
				TU_PVJ_ITEM(VarVec3);
				TU_PVJ_ITEM(VarVec4);
				TU_PVJ_ITEM(VarIVec2);
				TU_PVJ_ITEM(VarIVec3);
				TU_PVJ_ITEM(VarIVec4);
			}
#       undef TU_PVJ_ITEM
		}
	}
};


bool Parser::args(int argc, char **argv, VarMap& vars)
{
	// no args
	if(argc < 2) 
	{
		return true;
	}
	
	// First param is the exec
	std::vector<std::string> args;
	for(int i = 1; i < argc ; ++i)
	{
		args.push_back(argv[i]);
	}

	VarArgParser parser(args);
	parser.parse(vars);

	return true;
}
}