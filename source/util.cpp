#include <util.h>
#include <algorithm>
#include <string> 
#include <sstream>

namespace tunnel
{

std::string toLower(const std::string& str)
{
	std::string ret = str;
	std::transform(ret.begin(), ret.end(), ret.begin(), ::tolower);
	return ret;
}

std::string toUpper(const std::string& str)
{
	std::string ret = str;
	std::transform(ret.begin(), ret.end(), ret.begin(), ::toupper);
	return ret;
}

template <typename T> bool casting_system(const std::string& s , T& result)
{
	std::stringstream ss(s);
	return !((ss >> result).fail() || !(ss >> std::ws).eof());
}

bool stringTo(std::string value, int32_t& out)
{
	return casting_system(value, out);
}

bool stringTo(std::string value, uint32_t& out)
{
	return casting_system(value, out);
}

bool stringTo(std::string value, int64_t& out)
{
	return casting_system(value, out);
}

bool stringTo(std::string value, uint64_t& out)
{
	return casting_system(value, out);
}

bool stringTo(std::string value, float& out)
{
	return casting_system(value, out);
}

bool stringTo(std::string value, double& out)
{
	return casting_system(value, out);
}

}
