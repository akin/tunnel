#include <platform.h>

namespace tunnel
{
ILog *Platform::sm_log = nullptr;
VarMap Platform::sm_vars;

void Platform::set(ILog *log)
{
	sm_log = log;
}

ILog& Platform::getLog()
{
	return *sm_log;
}

VarMap& Platform::getVars()
{
	return sm_vars;
}

}

