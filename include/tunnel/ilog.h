#ifndef TUNNEL_ILOG_HEADER
#define TUNNEL_ILOG_HEADER

#include <cstddef>

namespace tunnel
{
	
enum class LogLevel
{
	None = 0,
	Message = 1,
	Warning = 2,
	Error = 3,
};
	
class ILog
{
protected:
	virtual void write(LogLevel level, size_t count, void *buf) = 0;
public:
	virtual ~ILog() {}
	
	void report(LogLevel level, const char *msg, ...);
};
}

#endif