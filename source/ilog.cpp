#include  <ilog.h>

#include <iostream>
#include <stdarg.h>
#include <stdio.h>
#include <vector>

#define MAX_LOG_MSG_LEN 4096

namespace tunnel
{
void ILog::report(LogLevel level, const char *msg, ...)
{
	std::vector<char> buffer;
	buffer.resize(MAX_LOG_MSG_LEN);

	va_list args;
	va_start(args, msg);
	int finalsize = vsnprintf(&buffer[0], (buffer.size() - 1), msg, args);
	va_end(args);

	if ((finalsize + 1) < buffer.size())
	{
		buffer.resize(finalsize + 1);
	}

	write(level, buffer.size(), &buffer[0]);
}

}
