#ifndef TUNNEL_PLATFORM_HEADER
#define TUNNEL_PLATFORM_HEADER

#include "config.h"
#include "ilog.h"
#include "varmap.h"

#define LOG_REPORT tunnel::Platform::getLog().report
#define LOG_SET(log) tunnel::Platform::set(&log)
#define VARS tunnel::Platform::getVars()

namespace tunnel
{
class Platform
{
private:
	static ILog *sm_log;
	static VarMap sm_vars;
public:
	static void set(ILog *log);
	static ILog& getLog();
	static VarMap& getVars();
};
}

#endif
