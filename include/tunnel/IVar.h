
#ifndef TUNNEL_IVAR_HEADER
#define TUNNEL_IVAR_HEADER

#include "types.h"

namespace tunnel
{
class IVar
{
private:
	Types::Type m_type;
protected:
	IVar(Types::Type type)
	: m_type(type) 
	{}
public:
	virtual ~IVar() {}
	Types::Type getType() const
	{ 
		return m_type;
	}
	static Types::Type getClassType()
	{
		return Types::none;
	}
};
}

#endif
