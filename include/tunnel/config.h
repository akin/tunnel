
#ifndef TUNNEL_CONFIG_HEADER
#define TUNNEL_CONFIG_HEADER

// Test to make Ownership of objects more clear
#define NoOwnershipOf(classType) classType*
#define OwnershipOf(classType) classType*

// Common defines:
#ifndef DELTA
# define DELTA 0.0001f
#endif
#ifndef PI
# define PI 3.141593f
#endif
#ifndef PI2
# define PI2 6.283185f
#endif
#ifndef DEG2RAD
# define DEG2RAD 0.017453292519943
#endif
// PI/360.0f =
#ifndef PI_OVER_360
# define PI_OVER_360 0.0087266462599716478846184538424431
#endif

#endif
