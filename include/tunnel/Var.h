
#ifndef TUNNEL_VAR_HEADER
#define TUNNEL_VAR_HEADER

#include "types.h"
#include <string>
#include <glm\glm.hpp>

namespace tunnel
{
template <typename CType, Types::Type signature>
class Var : public IVar
{
public:
	static const Types::Type types_type = signature;
private:
	CType m_value;
public:
	Var()
	: IVar(signature)
	{
	}
	Var(const CType& value)
	: IVar(signature)
	, m_value(value)
	{
	}
	void set(const CType& value)
	{
		m_value = value;
	}
	const CType& get() const
	{
		return m_value;
	}
	operator CType&() const
	{
		return m_value;
	}/* TODO! Why fails?
	operator=(const CType& value)
	{
		m_value = value;
	}*/
};

using VarInt32 = Var<int32_t, Types::int32>;
using VarUInt32 = Var<uint32_t, Types::uint32>;
using VarInt64 = Var<int64_t, Types::int64>;
using VarUInt64 = Var<uint64_t, Types::uint64>;
using VarBool = Var<bool, Types::boolean>;
using VarFloat32 = Var<float, Types::float32>;
using VarFloat64 = Var<double, Types::float64>;
using VarString = Var<std::string, Types::string>;
using VarVec2 = Var<glm::vec2, Types::vec2>;
using VarVec3 = Var<glm::vec3, Types::vec3>;
using VarVec4 = Var<glm::vec4, Types::vec4>;
using VarIVec2 = Var<glm::ivec2, Types::ivec2>;
using VarIVec3 = Var<glm::ivec3, Types::ivec3>;
using VarIVec4 = Var<glm::ivec4, Types::ivec4>;
}

#endif
