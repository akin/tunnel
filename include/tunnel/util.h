
#ifndef TUNNEL_UTIL_HEADER
#define TUNNEL_UTIL_HEADER

#include <string>

namespace tunnel
{
std::string toLower(const std::string& str);
std::string toUpper(const std::string& str);
bool stringTo(std::string value, int32_t& out);
bool stringTo(std::string value, uint32_t& out);
bool stringTo(std::string value, int64_t& out);
bool stringTo(std::string value, uint64_t& out);
bool stringTo(std::string value, float& out);
bool stringTo(std::string value, double& out);
}

#endif
