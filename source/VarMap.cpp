
#include  <varmap.h>
#include <util.h>

namespace tunnel
{
bool VarMap::has(std::string key) const
{
	std::string realkey = toLower(key);
	return m_value.find(realkey) != m_value.end();
}

void VarMap::apply(std::function<void(const std::string&, IVar *)> function)
{
	for (auto iter : m_value)
	{
		function(iter.first, iter.second);
	}
}

void VarMap::apply(std::function<void(const std::string&, const IVar *)> function) const
{
	for (auto iter : m_value)
	{
		function(iter.first, iter.second);
	}
}

Types::Type VarMap::getType(std::string key) const
{
	IVar *var = getVar(key);
	if (var == nullptr)
	{
		return Types::none;
	}
	return var->getType();
}

IVar *VarMap::getVar(const std::string& key) const
{
	std::string realkey = toLower(key);
	auto iter = m_value.find(realkey);
	if (iter == m_value.end())
	{
		return nullptr;
	}
	return iter->second;
}

void VarMap::setVar(const std::string& key, IVar *value)
{
	std::string realkey = toLower(key);
	IVar *var = getVar(realkey);
	if (var != nullptr)
	{
		/// oh no!
		delete var;
	}
	m_value[realkey] = value;
}
}